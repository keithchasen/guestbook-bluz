$(document).ready(function () {
//счетчик комментариев

    count();
    var id;
    var path;

    $('body')

        .on('click', '.showMore', function () {

            $(this).parents('.panel-default').find('.commentText').toggleClass('showText');

            if ($(this).html() == "Show less...") {
                $(this).html("Show more...");

            } else {

                $(this).html("Show less...");
            }
        })

        .on("click", ".delete", function () {
            dellMess(this);
        })

        .on("click", ".del", function () {
            delConfirm();
        })

        .on("click", ".add", function (e) {
            e.preventDefault();
            add();

        });

    function count() {
        var panels = $(".panel");
        var n = panels.length;
        $(".badge").text(n);
        panels.each(function () {
            $(this).find('.number').text('#' + (n));
            n--;
        });
    }

    function dellMess(el) {
        path = $(el).parents('.panel-default');
        id = path.find('.comment').attr('id');
        path.toggleClass("toDel");
    }

    function delConfirm() {

        $.ajax({
            type: 'POST',
            url: 'messages/delete/',
            data: {id: id},
            dataType: 'html',
            success: function () {
                $(".toDel").remove();
                count();
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText + '|\n' + status + '|\n' + error);
            }
        })
    }

    function add() {
        var formData = new FormData($('#formsend')[0]);

        $.ajax({
            type: 'POST',
            processData: false,
            contentType: false,
            url: 'messages/add/',
            dataType: 'html',
            data: formData,
            success: function (data, status, xhr) {
                if (!xhr.getResponseHeader("Bluz-Notify")) {
                    $("#sms").prepend(data);
                    count();
                    $('#formsend')[0].reset();
                }
            },
            error: function (xhr, status, error) {
                alert(xhr.responseText + '|\n' + status + '|\n' + error);
            }
        });
    }
});