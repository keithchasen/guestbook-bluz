<?php

namespace Application;

return

    /**
     * @return \closure
     */

    function () {

        /**
         * @var Bootstrap $this
         */

            $this->redirectTo('messages', 'index');

    };