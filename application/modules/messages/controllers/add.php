<?php

namespace Application;

use Application\Messages\Crud;
use Application\Messages\Table;
use Bluz\Proxy\Config;
use Bluz\Controller;
use Bluz\Proxy\Logger;
use Bluz\Proxy\Messages;
use Bluz\Proxy\Request;
use Bluz\Validator\Exception\ValidatorException;
use Image\Exception;
use Symfony\Component\Config\Exception\FileLoaderLoadException;
use Zend\Diactoros\UploadedFile;

return

    /**
     * @accept JSON
     * @accept HTML
     * @return mixed
     * @privilege Upload
     */

    function () use ($view) {

        /**
         * @var \Bluz\View\View $view
         * @var Bootstrap $this
         * @var UploadedFile $file
         */


        $userId = $this->user()->id;
        $filePath = null;

        $file = Request::getFile('fileToUpload');
        $message = Request::getParam('comment');

        if (!empty($file->getClientFilename())) {
            try {
                $crud = Crud::getInstance();

                //public/uploads
                $path = Config::getModuleData('messages', 'upload_path');

                if (empty($path)) {
                    throw new Exception('Upload path is not configured');
                }
                $crud->setUploadDir($path . '/messagePics');

                $filePath = $crud->uploadFile();
            } catch (\Exception $e) {
                Messages::addError('Error occurred while adding file.');
                Logger::getInstance()->log('error', $e->getMessage(), ['module' => 'messages', 'controller' => 'add']);
            }
        }

        try {
            $dataToInsert = ['user_id' => $userId, 'text' => $message, 'image' => $filePath];
            $view->fullMessage = Table::getInstance()->addMessage($dataToInsert);
        } catch (ValidatorException $e) {
            $this->useJson();
            foreach ($e->getErrors() as $errorInput) {
                foreach ($errorInput as $error) {
                    Messages::addError('Validation error: ' . $error);
                    Logger::getInstance()->log('error', $error, ['module' => 'messages', 'controller' => 'add']);
                }
            }
        } catch (\Exception $e) {
            Messages::addError('Error occurred while adding message.');
            Logger::getInstance()->log('error', $e->getMessage(), ['module' => 'messages', 'controller' => 'add']);
        }

    };