<?php

namespace Application;

use Application\Messages\Table;
use Bluz\Proxy\Messages;
use Bluz\Proxy\Logger;

return

    /**
     * @accept JSON
     * @accept HTML
     * @return \closure
     * @param int $id
     */

    function ($id) {
        try {
            $dataToDelete = ['id' => $id];
            Table::getInstance()->deleteMessage($dataToDelete);
        } catch (Exception $e) {
            Messages::addError('Error occurred while deleting message');
            Logger::getInstance()->log('error', $e->getMessage(), ['module' => 'messages', 'controller' => 'delete']);
        }
    };