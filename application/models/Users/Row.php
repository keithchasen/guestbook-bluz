<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Users;

use Application\Exception;
use Application\Privileges;
use Application\Roles;
use Bluz\Auth\AbstractRowEntity;
use Bluz\Auth\AuthException;
use Bluz\Validator\Traits\Validator;
use Bluz\Validator\Validator as v;
use Bluz\Proxy\Auth;
use Bluz\Proxy\Session;

/**
 * User
 *
 * @category Application
 * @package  Users
 *
 * @property string $name
 * @property string $email
 * @property string $password
 * @property string $created
 * @property string $updated
 * @property string $status
 *
 * @author   Anton Shevchuk
 * @created  08.07.11 17:13
 */
class Row extends AbstractRowEntity
{
    use Validator;

    /**
     * Small cache of user privileges
     * @var array
     */
    protected $privileges;

    /**
     * @return void
     */
    public function beforeSave()
    {
        $this->email = strtolower($this->email);

        $this->addValidator(
            'name',
            v::required()->latinNumeric()->length(3, 50),
            v::callback(function ($name) {
                $user = $this->getTable()
                    ->select()
                    ->where('name = ?', $name)
                    ->execute();
                return !$user;
            })->setError('User with login "{{input}}" already exists')
        );

        $this->addValidator(
            'email',
            v::required()->email(true),
            v::callback(function ($email) {
                $user = $this->getTable()
                    ->select()
                    ->where('email = ?', $email)
                    ->execute();
                return !$user;
            })->setError('User with email "{{input}}" already exists')
        );
    }

    /**
     * @return void
     */
    public function beforeInsert()
    {
        $this->created = gmdate('Y-m-d H:i:s');
        $this->password = md5($this->password);
    }

    /**
     * @return void
     */
    public function beforeUpdate()
    {
        $this->updated = gmdate('Y-m-d H:i:s');
    }

    /**
     * Can entity login
     *
     * @throws Exception
     * @throws AuthException
     * @return void
     */
    public function tryLogin()
    {
        // regenerate session
        if (PHP_SAPI !== 'cli') {
            Session::regenerateId();
        }
        // save user to new session
        Auth::setIdentity($this);
    }

    /**
     * Get user roles
     */
    public function getRoles()
    {
//        return Roles\Table::getInstance()->getUserRoles($this->id);
    }

    /**
     * Get user privileges
     */
    public function getPrivileges()
    {

    }

    /**
     * Check user role
     *
     * @param integer $roleId
     * @return boolean
     */
    public function hasRole($roleId)
    {

    }
}
