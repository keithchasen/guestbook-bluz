<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Users;

use Application\Auth;
use Application\Exception;
use Application\UsersActions;
//use Bluz\Proxy\Logger;
//use Bluz\Proxy\Mailer;
//use Bluz\Proxy\Messages;
//use Bluz\Proxy\Router;
use Bluz\Validator\Exception\ValidatorException;
use Bluz\Proxy\Messages;


/**
 * Crud
 *
 * @package  Application\Users
 *
 * @method   Table getTable()
 *
 * @author   Anton Shevchuk
 * @created  30.10.12 16:11
 */
class Crud extends \Bluz\Crud\Table
{
    protected $users;
    /**
     * @param array $data
     * @throws Exception
     * @throws ValidatorException
     * @return integer
     */
    public function createOne($data)
    {
        // password
        $password = isset($data['password'])?$data['password']:null;
        $password2 = isset($data['password2'])?$data['password2']:null;

        if (empty($password)) {
            throw ValidatorException::exception('password', __('Password can\'t be empty'));
        }

        if ($password !== $password2) {
            throw ValidatorException::exception('password2', __('Password is not equal'));
        }


        /** @var $row Row */
        $row = $this->getTable()->create();
        $row->setFromArray($data);
        $row->status = Table::STATUS_PENDING;
        $row->save();
        $userId = $row->id;
        $name = $row->name;
        // create auth
        Auth\Table::getInstance()->authenticateEquals($name, $password);
        Messages::addNotice('You are register!');

        app()->redirectTo('index', 'index');

        return $userId;
    }
}
