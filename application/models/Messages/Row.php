<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Messages;

use Application\Users;
use Bluz\Validator\Traits\Validator;
use Bluz\Validator\Validator as v;
use Image\Thumbnail;

/**
 * Media Row
 *
 * @category Application
 * @package  Messages
 *
 * @property integer $id
 * @property integer $userId
 * @property string $title
 * @property string $text
 * @property string $module
 * @property string $type
 * @property string $file
 * @property string $preview
 * @property string $created
 * @property string $updated
 */
class Row extends \Bluz\Db\Row
{
    use Validator;

    const THUMB_HEIGHT = 120;
    const THUMB_WIDTH = 120;

    /**
     * {@inheritdoc}
     */
    protected function beforeSave()
    {
        $this->addValidator(
            'text',
            v::required()->notEmpty()->setError('Message could not be empty'),
            v::string()->length(1, 5000)->setError('Message could not contain more than 5000 symbols')
            );
        
        $this->addValidator('user_id', v::required()->notEmpty());
    }

    /**
     * __insert
     *
     * @return void
     */
    protected function beforeInsert()
    {
        $this->created = date('Y-m-d H:i:s');
    }

    /**
     * __update
     *
     * @return void
     */
    protected function beforeUpdate()
    {
        $this->updated = gmdate('Y-m-d H:i:s');
    }

    /**
     * postDelete
     *
     * @return void
     */
    protected function afterDelete()
    {
        if (is_file(PATH_PUBLIC .'/'. $this->file)) {
            @unlink(PATH_PUBLIC .'/'. $this->file);
        }
        if (is_file(PATH_PUBLIC .'/'. $this->preview)) {
            @unlink(PATH_PUBLIC .'/'. $this->preview);
        }
    }
}
