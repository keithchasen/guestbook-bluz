<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Messages;

use Application\Exception;
use Bluz\Proxy\Request;

/**
 * Class Crud of Messages
 * @package Application\Messages
 *
 * @method Table getTable()
 */
class Crud extends \Bluz\Crud\Table
{
    /**
     * @var string
     */
    protected $uploadDir;

    /**
     * setUploadDir
     *
     * @param $directory
     * @throws Exception
     * @return self
     */
    public function setUploadDir($directory)
    {
        if (!is_dir($directory) && !@mkdir($directory, 0755, true)) {
            throw new Exception("Upload folder is not exists and I can't create it");
        }

        if (!is_writable($directory)) {
            throw new Exception("Upload folder is not writable");
        }

        $this->uploadDir = $directory;

        return $this;
    }

    /**
     * uploadFile
     *
     * @throws \Application\Exception
     * @return string
     */
    public function uploadFile()
    {
        /**
         * Process HTTP File
         */
        $file = Request::getFile('fileToUpload');

        if (!$file or $file->getError() != UPLOAD_ERR_OK) {
            if ($file->getError() == UPLOAD_ERR_NO_FILE) {
                throw new Exception("Please choose file for upload");
            }
            throw new Exception("Sorry, I can't receive file");
        }

        /**
         * Generate image name
         */
        $pathinfo = pathinfo($file->getClientFilename());

        $fileName = uniqid();
        $extension = $pathinfo['extension'];
        $fileInfo = $fileName .'.'. $extension;

        $filePath = $this->uploadDir .'/'. $fileInfo;

        // Setup new name and move to user directory
        $file->moveTo($filePath);
        return $fileInfo;
    }
}
