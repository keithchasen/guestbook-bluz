<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Messages;


/**
 * Class Table
 *
 * @package  Application\Messages
 *
 *
 * @method   static Row findRow($primaryKey)
 * @method   static Row findRowWhere($whereList)
 */
class Table extends \Bluz\Db\Table
{
    /**
     * Table
     *
     * @var string
     */
    protected $table = 'messages';
    protected $primary = array('id');
    
    /**
     * Get all messages 
     *
     * @return array
     */
    public function getAll()
    {
        $messages = $this->fetch('SELECT m.id, m.text, m.date, u.name, m.image, m.user_id
                             FROM messages m, users u 
                             WHERE u.id = m.user_id
                             ORDER BY 1 DESC');
        return $messages;
    }

    /**
     * Adding a message
     *
     * @param array $data
     * @return integer
     */

    public function addMessage($data)
    {
        $row = $this->create($data);
        $row->save();
        return $row;
    }
    
    /**
     * Deleting a message
     *
     * @param array $where
     */

    public function  deleteMessage($where)
    {
        $this->delete($where);
    }
}
