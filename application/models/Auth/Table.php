<?php
/**
 * @copyright Bluz PHP Team
 * @link https://github.com/bluzphp/skeleton
 */

/**
 * @namespace
 */
namespace Application\Auth;

use Application\Exception;
use Application\Users;
use Bluz\Application;
use Bluz\Auth\AbstractTable;
use Bluz\Auth\AuthException;
use Bluz\Proxy\Auth;
use Bluz\Proxy\Config;
use Bluz\Proxy\Response;

/**
 * Auth Table
 *
 * @package  Application\Auth
 *
 * @method   static Row findRow($primaryKey)
 * @method   static Row findRowWhere($whereList)
 *
 * @author   Anton Shevchuk
 * @created  12.07.11 15:28
 */
class Table extends \Bluz\Db\Table
{
    protected $table = 'users';
    protected $primary = ['id'];

    /**
     * Authenticate user by login/pass
     *
     * @param string $name
     * @param string $password
     * @throws AuthException
     * @throws Exception
     */
    public function authenticateEquals($name, $password)
    {
        if (!$authRow = $this->checkEquals($name, $password)) {
            throw new Exception("Invalid Name or Password! Please try again!!!");
        }

        // save user to new session
        Auth::setIdentity($authRow);
    }

    /**
     * Check user by login/pass
     *
     * @param string $name
     * @param string $password
     * @throws AuthException
     * @return Row
     */
    public function checkEquals($name, $password)
    {
        $authRow = $this->findRowWhere(['name' => $name]);


        if (!$authRow) {
            throw new AuthException("Wrong login or password!");
        }

        if (md5($password) !== $authRow->password) {
            throw new AuthException("Wrong password");
        }
        return $authRow;
    }

    public function generateCookie()
    {
        $cookie = Config::getData('cookie');
        $ttl = $cookie['rememberMe'];
        $row = $this->findRow(app()->user()->id);
        $row->expired = gmdate('Y-m-d H:i:s', time() + $ttl);
        $result = $row->save();
        if ($result) {
            setcookie('rId', app()->user()->id, time() + $ttl, '/');
        }
    }

    /**
     * Authenticate via cookie
     *
     * @param $userId
     * @throws AuthException
     * @throws Exception
     */
    public function authenticateCookie($userId)
    {
        $authRow = $this->checkCookie($userId);
        // get user row
        $user = Users\Table::findRow($authRow->id);
        // try to login
        $user->tryLogin();
    }

    /**
     * Check if supplied cookie is valid
     *
     * @param $userId
     * @return Row
     * @throws AuthException
     */
    public function checkCookie($userId)
    {
        if (!$authRow = $this->findRow($userId)) {
            throw new AuthException('User not found');
        }
        return $authRow;
    }

}


